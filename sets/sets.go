package sets

import (
	"../../byte_table"
)

func TestTableArrayAnyLinearSkitterDump() {
	dict := table.NewTableArrayAny()
	table.TableImportDisk(dict.TableInsertLinear, "../byte_table/data_skitter")
	dict.TableDump("capacity", "./capacityLinearInsertSkitter", "Skitter")
}

func TestTableArrayAnyLinearLivejournalDump() {
	dict := table.NewTableArrayAny()
	table.TableImportDisk(dict.TableInsertLinear, "../byte_table/data_livejournal")
	dict.TableDump("capacity", "./capacityLinearInsertLivejournal", "Livejournal")
}

func TestTableArrayAnyLinearOrkutDump() {
	dict := table.NewTableArrayAny()
	table.TableImportDisk(dict.TableInsertLinear, "../byte_table/data_orkut")
	dict.TableDump("capacity", "./capacityLinearInsertOrkut", "Orkut")
}

func TestTableArrayAnyLinearSkitterMemory() {
	dict := table.NewTableArrayAny()
	table.MemoryAllocated(func() {
		table.TableImportDisk(dict.TableInsertLinear, "../byte_table/data_skitter")
	}, "megabytes", "./megabytesLinearInsertSkitter", "Skitter")
}

func TestTableArrayAnyLinearSkitterInsert() {
	dict := table.NewTableArrayAny()
	table.Time(func() {
		table.TableImportDisk(dict.TableInsertLinear, "../byte_table/data_skitter")
	}, "time", "./timeLinearInsertSkitter", "Skitter")
}

func TestTableArrayAnyLinearSkitter() {
	dict := table.NewTableArrayAny()
	table.TableImportDisk(dict.TableInsertLinear, "../byte_table/data_skitter")
	table.Time(func() {
		table.TableBreadthSearch(dict.TableFindLinear, dict.TableRemoveLinear, []byte("0"))
	}, "time", "./timeLinearSearchSkitter", "Skitter")
}

func TestTableArrayAnyLinearLivejournalMemory() {
	dict := table.NewTableArrayAny()
	table.MemoryAllocated(func() {
		table.TableImportDisk(dict.TableInsertLinear, "../byte_table/data_livejournal")
	}, "megabytes", "./megabytesLinearInsertLivejournal", "Livejournal")
}

func TestTableArrayAnyLinearLivejournalInsert() {
	dict := table.NewTableArrayAny()
	table.Time(func() {
		table.TableImportDisk(dict.TableInsertLinear, "../byte_table/data_livejournal")
	}, "time", "./timeLinearInsertLivejournal", "Livejournal")
}

func TestTableArrayAnyLinearLivejournal() {
	dict := table.NewTableArrayAny()
	table.TableImportDisk(dict.TableInsertLinear, "../byte_table/data_livejournal")
	table.Time(func() {
		table.TableBreadthSearch(dict.TableFindLinear, dict.TableRemoveLinear, []byte("0"))
	}, "time", "./timeLinearSearchLivejournal", "Livejournal")
}

func TestTableArrayAnyLinearOrkutMemory() {
	dict := table.NewTableArrayAny()
	table.MemoryAllocated(func() {
		table.TableImportDisk(dict.TableInsertLinear, "../byte_table/data_orkut")
	}, "megabytes", "./megabytesLinearInsertOrkut", "Orkut")
}

func TestTableArrayAnyLinearOrkutInsert() {
	dict := table.NewTableArrayAny()
	table.Time(func() {
		table.TableImportDisk(dict.TableInsertLinear, "../byte_table/data_orkut")
	}, "time", "./timeLinearInsertOrkut", "Orkut")
}

func TestTableArrayAnyLinearOrkut() {
	dict := table.NewTableArrayAny()
	table.TableImportDisk(dict.TableInsertLinear, "../byte_table/data_orkut")
	table.Time(func() {
		table.TableBreadthSearch(dict.TableFindLinear, dict.TableRemoveLinear, []byte("1"))
	}, "time", "./timeLinearSearchOrkut", "Orkut")
}

func TestTableArrayAnyQuadraticSkitterDump() {
	dict := table.NewTableArrayAny()
	table.TableImportDisk(dict.TableInsertQuadratic, "../byte_table/data_skitter")
	dict.TableDump("capacity", "./capacityQuadraticInsertSkitter", "Skitter")
}

func TestTableArrayAnyQuadraticLivejournalDump() {
	dict := table.NewTableArrayAny()
	table.TableImportDisk(dict.TableInsertQuadratic, "../byte_table/data_livejournal")
	dict.TableDump("capacity", "./capacityQuadraticInsertLivejournal", "Livejournal")
}

func TestTableArrayAnyQuadraticOrkutDump() {
	dict := table.NewTableArrayAny()
	table.TableImportDisk(dict.TableInsertQuadratic, "../byte_table/data_orkut")
	dict.TableDump("capacity", "./capacityQuadraticInsertOrkut", "Orkut")
}

func TestTableArrayAnyQuadraticSkitterMemory() {
	dict := table.NewTableArrayAny()
	table.MemoryAllocated(func() {
		table.TableImportDisk(dict.TableInsertQuadratic, "../byte_table/data_skitter")
	}, "megabytes", "./megabytesQuadraticInsertSkitter", "Skitter")
}

func TestTableArrayAnyQuadraticSkitterInsert() {
	dict := table.NewTableArrayAny()
	table.Time(func() {
		table.TableImportDisk(dict.TableInsertQuadratic, "../byte_table/data_skitter")
	}, "time", "./timeQuadraticInsertSkitter", "Skitter")
}

func TestTableArrayAnyQuadraticSkitter() {
	dict := table.NewTableArrayAny()
	table.TableImportDisk(dict.TableInsertQuadratic, "../byte_table/data_skitter")
	table.Time(func() {
		table.TableBreadthSearch(dict.TableFindQuadratic, dict.TableRemoveQuadratic, []byte("0"))
	}, "time", "./timeQuadraticSearchSkitter", "Skitter")
}

func TestTableArrayAnyQuadraticLivejournalMemory() {
	dict := table.NewTableArrayAny()
	table.MemoryAllocated(func() {
		table.TableImportDisk(dict.TableInsertQuadratic, "../byte_table/data_livejournal")
	}, "megabytes", "./megabytesQuadraticInsertLivejournal", "Livejournal")
}

func TestTableArrayAnyQuadraticLivejournalInsert() {
	dict := table.NewTableArrayAny()
	table.Time(func() {
		table.TableImportDisk(dict.TableInsertQuadratic, "../byte_table/data_livejournal")
	}, "time", "./timeQuadraticInsertLivejournal", "Livejournal")
}

func TestTableArrayAnyQuadraticLivejournal() {
	dict := table.NewTableArrayAny()
	table.TableImportDisk(dict.TableInsertQuadratic, "../byte_table/data_livejournal")
	table.Time(func() {
		table.TableBreadthSearch(dict.TableFindQuadratic, dict.TableRemoveQuadratic, []byte("0"))
	}, "time", "./timeQuadraticSearchLivejournal", "Livejournal")
}

func TestTableArrayAnyQuadraticOrkutMemory() {
	dict := table.NewTableArrayAny()
	table.MemoryAllocated(func() {
		table.TableImportDisk(dict.TableInsertQuadratic, "../byte_table/data_orkut")
	}, "megabytes", "./megabytesQuadraticInsertOrkut", "Orkut")
}

func TestTableArrayAnyQuadraticOrkutInsert() {
	dict := table.NewTableArrayAny()
	table.Time(func() {
		table.TableImportDisk(dict.TableInsertQuadratic, "../byte_table/data_orkut")
	}, "time", "./timeQuadraticInsertOrkut", "Orkut")
}

func TestTableArrayAnyQuadraticOrkut() {
	dict := table.NewTableArrayAny()
	table.TableImportDisk(dict.TableInsertQuadratic, "../byte_table/data_orkut")
	table.Time(func() {
		table.TableBreadthSearch(dict.TableFindQuadratic, dict.TableRemoveQuadratic, []byte("1"))
	}, "time", "./timeQuadraticSearchOrkut", "Orkut")
}
