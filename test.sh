go build -o megabytesLinearInsertSkitter_ megabytesLinearInsertSkitter.go
./megabytesLinearInsertSkitter_
go build -o megabytesLinearInsertLivejournal_ megabytesLinearInsertLivejournal.go
./megabytesLinearInsertLivejournal_
go build -o megabytesLinearInsertOrkut_ megabytesLinearInsertOrkut.go
./megabytesLinearInsertOrkut_
go build -o capacityLinearInsertSkitter_ capacityLinearInsertSkitter.go
./capacityLinearInsertSkitter_
go build -o capacityLinearInsertLivejournal_ capacityLinearInsertLivejournal.go
./capacityLinearInsertLivejournal_
go build -o capacityLinearInsertOrkut_ capacityLinearInsertOrkut.go
./capacityLinearInsertOrkut_
go build -o timeLinearInsertSkitter_ timeLinearInsertSkitter.go
./timeLinearInsertSkitter_
go build -o timeLinearInsertLivejournal_ timeLinearInsertLivejournal.go
./timeLinearInsertLivejournal_
go build -o timeLinearInsertOrkut_ timeLinearInsertOrkut.go
./timeLinearInsertOrkut_
go build -o timeLinearSearchSkitter_ timeLinearSearchSkitter.go
./timeLinearSearchSkitter_
go build -o timeLinearSearchLivejournal_ timeLinearSearchLivejournal.go
./timeLinearSearchLivejournal_
go build -o timeLinearSearchOrkut_ timeLinearSearchOrkut.go
./timeLinearSearchOrkut_
go build -o megabytesQuadraticInsertSkitter_ megabytesQuadraticInsertSkitter.go
./megabytesQuadraticInsertSkitter_
go build -o megabytesQuadraticInsertLivejournal_ megabytesQuadraticInsertLivejournal.go
./megabytesQuadraticInsertLivejournal_
go build -o megabytesQuadraticInsertOrkut_ megabytesQuadraticInsertOrkut.go
./megabytesQuadraticInsertOrkut_
go build -o capacityQuadraticInsertSkitter_ capacityQuadraticInsertSkitter.go
./capacityQuadraticInsertSkitter_
go build -o capacityQuadraticInsertLivejournal_ capacityQuadraticInsertLivejournal.go
./capacityQuadraticInsertLivejournal_
go build -o capacityQuadraticInsertOrkut_ capacityQuadraticInsertOrkut.go
./capacityQuadraticInsertOrkut_
go build -o timeQuadraticInsertSkitter_ timeQuadraticInsertSkitter.go
./timeQuadraticInsertSkitter_
go build -o timeQuadraticInsertLivejournal_ timeQuadraticInsertLivejournal.go
./timeQuadraticInsertLivejournal_
go build -o timeQuadraticInsertOrkut_ timeQuadraticInsertOrkut.go
./timeQuadraticInsertOrkut_
go build -o timeQuadraticSearchSkitter_ timeQuadraticSearchSkitter.go
./timeQuadraticSearchSkitter_
go build -o timeQuadraticSearchLivejournal_ timeQuadraticSearchLivejournal.go
./timeQuadraticSearchLivejournal_
go build -o timeQuadraticSearchOrkut_ timeQuadraticSearchOrkut.go
./timeQuadraticSearchOrkut_
cat timeLinearInsertSkitter timeLinearInsertLivejournal timeLinearInsertOrkut > timeInsertLinear
cat timeQuadraticInsertSkitter timeQuadraticInsertLivejournal timeQuadraticInsertOrkut > timeInsertQuadratic
cat timeLinearSearchSkitter timeLinearSearchLivejournal timeLinearSearchOrkut > timeSearchLinear
cat timeQuadraticSearchSkitter timeQuadraticSearchLivejournal timeQuadraticSearchOrkut > timeSearchQuadratic
cat megabytesLinearInsertSkitter megabytesLinearInsertLivejournal megabytesLinearInsertOrkut > megabytesInsertLinear
cat megabytesQuadraticInsertSkitter megabytesQuadraticInsertLivejournal megabytesQuadraticInsertOrkut > megabytesInsertQuadratic
cat capacityLinearInsertSkitter capacityLinearInsertLivejournal capacityLinearInsertOrkut > capacityInsertLinear
cat capacityQuadraticInsertSkitter capacityQuadraticInsertLivejournal capacityQuadraticInsertOrkut > capacityInsertQuadratic
# replace.py https://github.com/no-glue/replace
# file_size.py https://github.com/no-glue/file_size
# gnuplot_helper_main.py https://github.com/no-glue/gnuplot_helper
replace.py -fileName timeInsertLinear -toReplace Skitter -replaceWith `file_size.py -format mb -file ../byte_table/data_skitter`
replace.py -fileName timeInsertLinear -toReplace Livejournal -replaceWith `file_size.py -format mb -file ../byte_table/data_livejournal`
replace.py -fileName timeInsertLinear -toReplace Orkut -replaceWith `file_size.py -format mb -file ../byte_table/data_orkut`
replace.py -fileName timeInsertQuadratic -toReplace Skitter -replaceWith `file_size.py -format mb -file ../byte_table/data_skitter`
replace.py -fileName timeInsertQuadratic -toReplace Livejournal -replaceWith `file_size.py -format mb -file ../byte_table/data_livejournal`
replace.py -fileName timeInsertQuadratic -toReplace Orkut -replaceWith `file_size.py -format mb -file ../byte_table/data_orkut`
replace.py -fileName timeSearchLinear -toReplace Skitter -replaceWith `file_size.py -format mb -file ../byte_table/data_skitter`
replace.py -fileName timeSearchLinear -toReplace Livejournal -replaceWith `file_size.py -format mb -file ../byte_table/data_livejournal`
replace.py -fileName timeSearchLinear -toReplace Orkut -replaceWith `file_size.py -format mb -file ../byte_table/data_orkut`
replace.py -fileName timeSearchQuadratic -toReplace Skitter -replaceWith `file_size.py -format mb -file ../byte_table/data_skitter`
replace.py -fileName timeSearchQuadratic -toReplace Livejournal -replaceWith `file_size.py -format mb -file ../byte_table/data_livejournal`
replace.py -fileName timeSearchQuadratic -toReplace Orkut -replaceWith `file_size.py -format mb -file ../byte_table/data_orkut`
replace.py -fileName megabytesInsertLinear -toReplace Skitter -replaceWith `file_size.py -format mb -file ../byte_table/data_skitter`
replace.py -fileName megabytesInsertLinear -toReplace Livejournal -replaceWith `file_size.py -format mb -file ../byte_table/data_livejournal`
replace.py -fileName megabytesInsertLinear -toReplace Orkut -replaceWith `file_size.py -format mb -file ../byte_table/data_orkut`
replace.py -fileName megabytesInsertQuadratic -toReplace Skitter -replaceWith `file_size.py -format mb -file ../byte_table/data_skitter`
replace.py -fileName megabytesInsertQuadratic -toReplace Livejournal -replaceWith `file_size.py -format mb -file ../byte_table/data_livejournal`
replace.py -fileName megabytesInsertQuadratic -toReplace Orkut -replaceWith `file_size.py -format mb -file ../byte_table/data_orkut`
replace.py -fileName capacityInsertLinear -toReplace Skitter -replaceWith `file_size.py -format mb -file ../byte_table/data_skitter`
replace.py -fileName capacityInsertLinear -toReplace Livejournal -replaceWith `file_size.py -format mb -file ../byte_table/data_livejournal`
replace.py -fileName capacityInsertLinear -toReplace Orkut -replaceWith `file_size.py -format mb -file ../byte_table/data_orkut`
replace.py -fileName capacityInsertQuadratic -toReplace Skitter -replaceWith `file_size.py -format mb -file ../byte_table/data_skitter`
replace.py -fileName capacityInsertQuadratic -toReplace Livejournal -replaceWith `file_size.py -format mb -file ../byte_table/data_livejournal`
replace.py -fileName capacityInsertQuadratic -toReplace Orkut -replaceWith `file_size.py -format mb -file ../byte_table/data_orkut`
gnuplot_helper_main.py -out timeInsert.png -skipColumns 1 -terminal "png" -files timeInsertLinear timeInsertQuadratic
cp timeInsert.png ~/Downloads/paper_pics
gnuplot_helper_main.py -out timeSearch.png -skipColumns 1 -terminal "png" -files timeSearchLinear timeSearchQuadratic
cp timeSearch.png ~/Downloads/paper_pics
gnuplot_helper_main.py -out megabytesInsert.png -skipColumns 1 -terminal "png" -files megabytesInsertLinear megabytesInsertQuadratic
cp megabytesInsert.png ~/Downloads/paper_pics
gnuplot_helper_main.py -out capacityInsert.png -skipColumns 1 -terminal "png" -files capacityInsertLinear capacityInsertQuadratic
cp capacityInsert.png ~/Downloads/paper_pics
