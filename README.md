# Dictionary benchmark
Benchmark for [dictionary](https://bitbucket.org/npavlicevic/byte_table)
## Before running benchmark
+ install go
+ install python
+ install gnuplot
+ add following to PATH
  + [file_size](https://github.com/no-glue/file_size)
  + [replace](https://github.com/no-glue/replace)
  + [gnuplot_helper](https://github.com/no-glue/gnuplot_helper)
## Benchmark
sh test.sh
